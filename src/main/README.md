# twig-lexer [![NPM version][npm-image]][npm-url] [![Build Status][build-image]][build-url] [![Coverage percentage][coveralls-image]][coveralls-url]

> A lossless Twig lexer

## Installation

```shell
npm install twig-lexer --save-dev
```

## Usage

```typescript
import {Lexer} from 'twig-lexer';

const lexer = new Lexer();

const tokens = lexer.tokenize('Hello {{world}}!');
```

## API

Read the [documentation](https://nightlycommit.github.io/twig-lexer) for more information.

## About Twig specifications - or lack thereof

As incredible as it sounds, Twig is a language with no official specifications - even internally at SensioLabs, [it seems](https://github.com/twigphp/Twig/issues/3066#issuecomment-502672166). As such, it is subject to interpretations and twig-lexer is one of them. It's very close to TwigPHP lexer (and as such implements things like [the operator confusion](https://github.com/twigphp/Twig/issues/3066)) but also outputs some token types that are not output by TwigPHP lexer - like `OPENING_QUOTE` or `WHITESPACE` - or with different names - like `TAG_START` instead of `BLOCK_START`.

When (if) official specifications are available, twig-lexer will be updated to match them.

## License

Apache-2.0 © [Eric MORAND]()

[npm-image]: https://badge.fury.io/js/twig-lexer.svg
[npm-url]: https://npmjs.org/package/twig-lexer
[build-image]: https://gitlab.com/nightlycommit/twig-lexer/badges/master/pipeline.svg
[build-url]: https://gitlab.com/nightlycommit/twig-lexer/-/pipelines
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/twig-lexer/badge.svg
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/twig-lexer