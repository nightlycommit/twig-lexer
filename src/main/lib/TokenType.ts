export type TokenType =
    "CLOSING_QUOTE" |
    "COMMENT_END" |
    "COMMENT_START" |
    "EOF" |
    "INTERPOLATION_START" |
    "INTERPOLATION_END" |
    "LINE_TRIMMING_MODIFIER" |
    "NAME" |
    "NUMBER" |
    "OPENING_QUOTE" |
    "OPERATOR" |
    "PUNCTUATION" |
    "SPREAD_OPERATOR" |
    "STRING" |
    "TAG_END" |
    "TAG_START" |
    "TEST_OPERATOR" |
    "TEXT" |
    "TRIMMING_MODIFIER" |
    "VARIABLE_END" |
    "VARIABLE_START" |
    "WHITESPACE" |
    "ARROW"
    ;